﻿namespace ConsoleApp1.SJMS.ZRL
{
    using System;
    /// <summary>
    /// 责任链模式
    /// 在C#数采系统中，‌责任链模式（‌Chain of Responsibility Pattern）‌是一种设计模式，‌它将请求的发送者和接收者解耦，‌通过给多个对象一个机会来处理这个请求。‌将这些对象连成一条链，‌并沿着这条链传递该请求，‌直到有一个对象处理它为止。‌
    /// 以下是一个简单的责任链模式实现示例，‌适用于数据采集系统：‌
    /// </summary>


    /// <summary>
    /// 1.‌定义处理器接口
    /// </summary>
    public interface IDataHandler
    {
        IDataHandler NextHandler { get; set; }
        void HandleData(float data);
    }

}

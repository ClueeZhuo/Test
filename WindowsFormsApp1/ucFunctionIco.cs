﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class ucFunctionIco : UserControl
    {
        public delegate void ucFunctionIcoHandler(object sender, EventArgs e);
        public event ucFunctionIcoHandler onIconClick;
        public ucFunctionIco()
        {
            InitializeComponent();
        }
        private string labTitleText = "";
        /// <summary>
        /// 标题
        /// </summary>
        public string ucTitleText
        {
            get
            {
                return labTitleText;
            }
            set
            {
                this.Title.Text = value;
            }
        }
        private Image picBoxBool;
        /// <summary>
        /// 角标
        /// </summary>
        public Image ucpicBoxBool
        {
            get
            {
                return picBoxBool;
            }
            set
            {
                this.PicBox_Bool.Image = value;
            }
        }
        private Image picBoxImg;
        /// <summary>
        /// 图标
        /// </summary>
        public Image ucpicBoxImg
        {
            get
            {
                return picBoxImg;
            }
            set
            {
                this.PicBox_Img.Image = value;
            }
        }


        private object _selected;

        public object Selected
        {
            get { return _selected; }
            set
            {
                if (!Equals(_selected, value))
                {
                    _selected = value;
                    OnSelectedChanged(EventArgs.Empty);
                }
            }
        }
        public event EventHandler Clicks;

        protected virtual void OnSelectedChanged(EventArgs e)
        {
            base.Click += ucFunctionIco_Click;
        }

        private void ucFunctionIco_Click(object sender, EventArgs e)
        {
            if (onIconClick != null)
            {
                onIconClick(this, e);
            }
        }
    }
}

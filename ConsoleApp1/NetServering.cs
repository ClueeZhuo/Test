﻿using NewLife.Data;
using NewLife.Log;
using NewLife.Net;
using NewLife.Net.Handlers;

namespace ConsoleApp1
{
    public class NetServering
    {
        public void Startup()
        {

            XTrace.UseConsole();
            var svr = new NetServer
            {
                Port = 1234,
                Log = XTrace.Log
            };

            svr.Received += Svr_Received;
            svr.Add<StandardCodec>();
            //svr.Add<EchoHandler>();

            // 打开原始数据日志
            var ns = svr.Server;
            ns.LogSend = true;
            ns.LogReceive = true;
            svr.Start();

            MessageActor._NetServer = svr;
            Console.ReadKey();
        }

        void Svr_Received(object? sender, ReceivedEventArgs e)
        {
            var pk = e.Message as Packet;

            XTrace.WriteLine("Svr_Received-收到：{0}", pk?.ToStr());

            var actor = new MessageActor();
            actor.Tell(pk?.ToStr());
            // 等待最终完成
            actor.Stop();
        }


    }
}

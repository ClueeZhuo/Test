﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.SJMS.GCZ
{
    /// <summary>
    /// 观察者模式
    /// 在C#数采系统中，‌观察者模式（‌Observer Pattern）‌是一种非常有用的设计模式，‌它允许一个对象（‌称为“主体”或“可观察者”）‌在状态发生变化时通知一系列依赖它的对象（‌称为“观察者”）‌。‌以下是一个简单的观察者模式实现示例，‌适用于数据采集系统：‌
    /// </summary>

    /// <summary>
    /// 1.‌定义观察者接口
    /// </summary>
    public interface IObserver
    {
        void Update(float temperature);
    }

}

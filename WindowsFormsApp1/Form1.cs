﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            for (int i = 1; i <= 3; i++)
            {
                var abc = new ucFunctionIco();
                abc.Name = i.ToString();
                abc.onIconClick += Abc_onIconClick;

                flowLayoutPanel1.Controls.Add(abc);
            }
        }

        private void Abc_onIconClick(object sender, EventArgs e)
        {
            var abc = (ucFunctionIco)sender;

            MessageBox.Show(abc.Name);
        }
    }
}

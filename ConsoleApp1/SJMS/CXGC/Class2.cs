﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.SJMS.CXGC
{
    /// <summary>
    /// 2.‌实现具体产品
    /// </summary>

    public class SensorDataCollector : IDataCollector
    {
        public void CollectData()
        {
            Console.WriteLine("Collecting data from sensors.");
        }
    }

    public class DatabaseDataCollector : IDataCollector
    {
        public void CollectData()
        {
            Console.WriteLine("Collecting data from database.");
        }
    }

    public class SensorDataProcessor : IDataProcessor
    {
        public void ProcessData()
        {
            Console.WriteLine("Processing sensor data.");
        }
    }

    public class DatabaseDataProcessor : IDataProcessor
    {
        public void ProcessData()
        {
            Console.WriteLine("Processing database data.");
        }
    }


}

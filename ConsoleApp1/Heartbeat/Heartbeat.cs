﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TouchSocket.Core;
using TouchSocket.Sockets;

namespace ConsoleApp1.Heartbeat
{
    #region 数据格式解析

    internal class MyFixedHeaderDataHandlingAdapter : CustomFixedHeaderDataHandlingAdapter<MyRequestInfo>
    {
        public override int HeaderLength => 3;

        public override bool CanSendRequestInfo => false;

        protected override MyRequestInfo GetInstance()
        {
            return new MyRequestInfo();
        }
    }

    internal class MyRequestInfo : IFixedHeaderRequestInfo
    {
        public DataType DataType { get; set; }
        public byte[] Data { get; set; }

        public int BodyLength { get; private set; }

        public bool OnParsingBody(ReadOnlySpan<byte> body)
        {
            if (body.Length == this.BodyLength)
            {
                this.Data = body.ToArray();
                return true;
            }
            return false;
        }

        public bool OnParsingHeader(ReadOnlySpan<byte> header)
        {
            if (header.Length == 3)
            {
                this.BodyLength = TouchSocketBitConverter.Default.To<ushort>(header) - 1;
                this.DataType = (DataType)header[2];
                return true;
            }
            return false;
        }

        public void Package(ByteBlock byteBlock)
        {
            byteBlock.WriteUInt16((ushort)((this.Data == null ? 0 : this.Data.Length) + 1));
            byteBlock.WriteByte((byte)this.DataType);
            if (this.Data != null)
            {
                byteBlock.Write(this.Data);
            }
        }

        public byte[] PackageAsBytes()
        {
            using var byteBlock = new ByteBlock();
            this.Package(byteBlock);
            return byteBlock.ToArray();
        }

        public override string ToString()
        {
            return $"数据类型={this.DataType}，数据={(this.Data == null ? "null" : Encoding.UTF8.GetString(this.Data))}";
        }
    }

    internal enum DataType : byte
    {
        Ping,
        Pong,
        Data
    }

    #endregion 数据格式解析

    /// <summary>
    /// 一个心跳计数器扩展。
    /// </summary>
    internal static class DependencyExtensions
    {
        public static readonly DependencyProperty<Timer> HeartbeatTimerProperty =
            new("HeartbeatTimer", null);

        public static async Task<bool> PingAsync<TClient>(this TClient client) where TClient : ISender, ILoggerObject
        {
            try
            {
                //await client.SendAsync(new MyRequestInfo() { DataType = DataType.Ping }.PackageAsBytes());

                //await client.SendAsync($"{(char)0}");

                await client.SendAsync($"PING");
                return true;
            }
            catch (Exception ex)
            {
                client.Logger.Exception(ex);
            }

            return false;
        }

        public static async Task<bool> PongAsync<TClient>(this TClient client) where TClient : ISender, ILoggerObject
        {
            try
            {
                await client.SendAsync(new MyRequestInfo() { DataType = DataType.Pong }.PackageAsBytes());
                return true;
            }
            catch (Exception ex)
            {
                client.Logger.Exception(ex);
            }

            return false;
        }
    }

    internal class HeartbeatAndReceivePlugin : PluginBase, ITcpConnectedPlugin, ITcpClosedPlugin, ITcpReceivedPlugin
    {
        private readonly int m_timeTick;
        private readonly TouchSocket.Core.ILog m_logger;

        [DependencyInject]
        public HeartbeatAndReceivePlugin(TouchSocket.Core.ILog logger, int timeTick = 1000 * 5)
        {
            this.m_timeTick = timeTick;
            this.m_logger = logger;
        }

        public async Task OnTcpConnected(ITcpSession client, ConnectedEventArgs e)
        {
            var type= client.GetType();

            //此处可判断，如果不是客户端，则不用使用心跳。
            if (client.IsClient && client is ITcpClient tcpClient)
            {
                if (client.GetValue(DependencyExtensions.HeartbeatTimerProperty) is Timer timer)
                {
                    timer.Dispose();
                }

                client.SetValue(DependencyExtensions.HeartbeatTimerProperty, new Timer(async (o) =>
                {
                    await tcpClient.PingAsync();
                }, null, 0, this.m_timeTick));
            }
            else if (!client.IsClient && client is TcpSessionClient tcpServer)
            {
                if (client.GetValue(DependencyExtensions.HeartbeatTimerProperty) is Timer timer)
                {
                    timer.Dispose();
                }

                client.SetValue(DependencyExtensions.HeartbeatTimerProperty, new Timer(async (o) =>
                {
                    await tcpServer.PingAsync();
                }, null, 0, this.m_timeTick));
            }


            await e.InvokeNext();
        }

        public async Task OnTcpClosed(ITcpSession client, ClosedEventArgs e)
        {
            if (client.GetValue(DependencyExtensions.HeartbeatTimerProperty) is Timer timer)
            {
                timer.Dispose();
                client.SetValue(DependencyExtensions.HeartbeatTimerProperty, null);
            }

            await e.InvokeNext();
        }

        public async Task OnTcpReceived(ITcpSession client, ReceivedDataEventArgs e)
        {
            if (e.RequestInfo is MyRequestInfo myRequest)
            {
                this.m_logger.Info(myRequest.ToString());

                if (client is ITcpClient tcpClient)
                {
                    if (myRequest.DataType == DataType.Ping)
                    {
                        await tcpClient.PongAsync();
                    }
                }

            }
            await e.InvokeNext();
        }
    }
}

﻿global using ConsoleApp1;
global using ConsoleApp1.Adapter;
global using ConsoleApp1.Heartbeat;

global using DynamicExpresso;

global using HslCommunication;
global using HslCommunication.MQTT;

global using InfluxDB.Client;
global using InfluxDB.Client.Api.Domain;
global using InfluxDB.Client.Core;
global using InfluxDB.Client.Linq;
global using InfluxDB.Client.Writes;

global using NewLife;
global using NewLife.Caching;
global using NewLife.Data;
global using NewLife.Log;
global using NewLife.Net;
global using NewLife.Net.Handlers;
global using NewLife.Serialization;

global using System.Collections.Concurrent;
global using System.Diagnostics;
global using System.Globalization;
global using System.IO.Ports;
global using System.Net;
global using System.Text;

global using TouchSocket.Core;
global using TouchSocket.Sockets;

global using static ConsoleApp1.Test.Class0;


global using static ConsoleApp1.Test.Class7;
global using static ConsoleApp1.Test.Class8;
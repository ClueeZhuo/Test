﻿using Grpc.Core;

using GrpcLibrary;

using System;

namespace GrpcClient
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Channel channel = new Channel("127.0.0.1:9007", ChannelCredentials.Insecure);
            var client = new GrpcService.GrpcServiceClient(channel);

            while (true)
            {
                var response = client.SayHello(new HelloRequest { Name = "Cluee" });
                Console.WriteLine("响应：" + response.Message);
            }

            channel.ShutdownAsync().Wait();
            Console.WriteLine("任意键退出...");
            Console.ReadKey();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.SJMS.CL
{

    /// <summary>
    /// 4.‌使用示例
    /// 在这个例子中，‌IDataCollectionStrategy 接口定义了所有策略应该实现的方法。‌SensorDataCollectionStrategy 和 DatabaseDataCollectionStrategy 类实现了这个接口，‌分别提供了从传感器和数据库采集数据的策略。‌DataCollector 类是上下文，‌它接受一个策略对象，‌并根据这个策略对象来采集数据。‌通过 SetStrategy 方法，‌可以在运行时更改采集数据的策略。‌
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            DataCollector collector = new DataCollector(new SensorDataCollectionStrategy());
            collector.Collect();

            collector.SetStrategy(new DatabaseDataCollectionStrategy());
            collector.Collect();
        }
    }

}

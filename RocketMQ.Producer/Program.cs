﻿using NewLife.Log;
using NewLife.RocketMQ;

using System.Diagnostics;

try
{
    XTrace.UseConsole();

    using var mq = new Producer
    {
        Topic = "SXHDMES_OPENMES_CS",
    };
    mq.Configure(MqSetting.Current);

    mq.Log = XTrace.Log;

    var s = mq.Start();

    //for (var i = 0; i < 10; i++)
    //{
    //    var str = "学无先后达者为师" + i;
    //    //var str = Rand.NextString(1337);

    //    var sr = mq.Publish(str, "LOGIN");
    //}

    while (true)
    {
        var str = "学无先后达者为师:" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

        var sr = mq.Publish(str, "LOGIN");
        Thread.Sleep(1000 * 2);
    }
}
catch (Exception exp)
{
    XTrace.WriteException(exp);
}
finally
{
    Console.ReadKey();
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.SJMS.CXGC
{
    /// <summary>
    /// 5.‌使用示例
    /// 在这个例子中，‌IDataCollector 和 IDataProcessor 是抽象产品接口，‌SensorDataCollector、‌DatabaseDataCollector、‌SensorDataProcessor 和 DatabaseDataProcessor 是具体的产品实现。‌IDataFactory 是抽象工厂接口，‌SensorDataFactory 和 DatabaseDataFactory 是具体的工厂实现。‌通过抽象工厂模式，‌我们可以在运行时根据需要创建不同家族的产品对象。‌
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            IDataFactory sensorFactory = new SensorDataFactory();
            IDataCollector sensorCollector = sensorFactory.CreateDataCollector();
            IDataProcessor sensorProcessor = sensorFactory.CreateDataProcessor();

            sensorCollector.CollectData();
            sensorProcessor.ProcessData();

            IDataFactory databaseFactory = new DatabaseDataFactory();
            IDataCollector databaseCollector = databaseFactory.CreateDataCollector();
            IDataProcessor databaseProcessor = databaseFactory.CreateDataProcessor();

            databaseCollector.CollectData();
            databaseProcessor.ProcessData();
        }
    }

}

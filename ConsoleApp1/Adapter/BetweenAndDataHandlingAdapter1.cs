﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TouchSocket.Core;

namespace ConsoleApp1.Adapter
{
    public class BetweenAndDataHandlingAdapter1 : CustomBetweenAndDataHandlingAdapter<BetweenAndRequestInfo1>
    {
        public BetweenAndDataHandlingAdapter1()
        {
            this.MinSize = 5;//表示，实际数据体不会小于5，例如“**12##12##”数据，解析后会解析成“12##12”

            this.m_startCode = Encoding.UTF8.GetBytes("**");//可以为0长度字节，意味着没有起始标识。
            this.m_endCode = Encoding.UTF8.GetBytes("##");//必须为有效值。
        }

        private readonly byte[] m_startCode;
        private readonly byte[] m_endCode;

        public override byte[] StartCode => m_startCode;

        public override byte[] EndCode => m_endCode;

        protected override BetweenAndRequestInfo1 GetInstance()
        {
            return new BetweenAndRequestInfo1();
        }
    }
}

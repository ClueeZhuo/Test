﻿using NAudio.Wave;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class NAudioService
    {
        private static readonly object locker = new object();

        WaveOutEvent currentWaveOut;

        public async Task Test3()
        {
            StopCurrentAudio();
            var time = 1;
            while (true)
            {
                try
                {
                    string audioUrl = $"https://inner-gw.hisense.com/hiai/tts?user_key=05d4388fc6d3a44ddfa1dae9ebfe813d&type=mp3&text=当前生产机型ABC批次{time} Thread{Thread.CurrentThread.ManagedThreadId}";

                    using (HttpClient client = new HttpClient())
                    {
                        Console.WriteLine("开始下载音频...");
                        var response = await client.GetAsync(audioUrl);
                        if (!response.IsSuccessStatusCode)
                        {
                            Console.WriteLine($"请求失败，状态码: {response.StatusCode}");
                            return;
                        }
                        byte[] audioData = await response.Content.ReadAsByteArrayAsync();
                        Console.WriteLine("音频下载完成。");

                        using (MemoryStream stream = new MemoryStream(audioData))
                        {
                            using (Mp3FileReader mp3Reader = new Mp3FileReader(stream))
                            {
                                using (currentWaveOut = new WaveOutEvent())
                                {
                                    StopCurrentAudio();

                                    currentWaveOut.Init(mp3Reader);

                                    currentWaveOut.Play();
                                    while (true)
                                    {
                                        if (currentWaveOut.PlaybackState != PlaybackState.Playing)
                                        {
                                            break;
                                        }
                                        else
                                        {
                                            await Task.Delay(500);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    time++;
                }
                catch (Exception ex)
                {
                    XTrace.WriteException(ex);
                }
            }
        }

        public void StopCurrentAudio()
        {
            if (currentWaveOut != null && currentWaveOut.PlaybackState == PlaybackState.Playing)
            {
                Console.Write("StopCurrentAudio");
                currentWaveOut.Stop();
                currentWaveOut.Dispose();
                currentWaveOut = null;
            }
        }
    }
}

﻿using NewLife.Log;

using System.IO.Ports;

namespace ConsoleApp1
{
    public class SerialPortHealthChecker
    {
        private SerialPort serialPort;

        public SerialPortHealthChecker(string portName)
        {
            serialPort = new SerialPort(portName);
        }

        // 尝试打开串口
        public bool TryOpenPort()
        {
            try
            {
                if (!serialPort.IsOpen)
                {
                    serialPort.Open();
                }
               
                XTrace.WriteLine("串口已打开。");
                return true;
            }
            catch (Exception ex)
            {
                XTrace.WriteLine("打开串口时发生错误: " + ex.Message);
                return false;
            }
        }

        // 关闭串口
        public void ClosePort()
        {
            if (serialPort.IsOpen)
            {
                serialPort.Close();
                XTrace.WriteLine("串口已关闭。");
            }
        }

        // 检查串口配置
        public bool CheckPortSettings(int baudRate, Parity parity, int dataBits, StopBits stopBits)
        {
            // 这里可以添加额外的逻辑来检查当前配置与期望的配置是否匹配
            // 例如：serialPort.BaudRate == baudRate 等
            // 但由于设置串口参数通常会导致串口关闭和重新打开，这里只演示如何设置参数

            try
            {
                serialPort.BaudRate = baudRate;
                serialPort.Parity = parity;
                serialPort.DataBits = dataBits;
                serialPort.StopBits = stopBits;
                XTrace.WriteLine("串口配置已更新。");
                return true;
            }
            catch (Exception ex)
            {
                XTrace.WriteLine("设置串口参数时发生错误: " + ex.Message);
                return false;
            }
        }

        // 发送测试数据并检查是否能接收
        public bool TestCommunication()
        {
            if (!serialPort.IsOpen)
            {
                XTrace.WriteLine("串口未打开，请先打开串口。");
                return false;
            }

            string testData = "TEST";
            try
            {
                // 发送测试数据
                serialPort.WriteLine(testData);

                // 等待并接收数据，这里假设数据接收逻辑已经实现
                string receivedData = serialPort.ReadLine();
                if (receivedData == testData)
                {
                    XTrace.WriteLine("数据接收成功。");
                    return true;
                }
                else
                {
                    XTrace.WriteLine("数据接收失败，接收到的数据与发送的数据不匹配。");
                    return false;
                }

                // 由于数据接收通常是异步的，这里仅演示发送数据
                XTrace.WriteLine("测试数据已发送。");
                return true;
            }
            catch (Exception ex)
            {
                XTrace.WriteLine("通信测试时发生错误: " + ex.Message);
                return false;
            }
        }
    }

}

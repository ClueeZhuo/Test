﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.SJMS.CL
{
    /// <summary>
    /// 策略模式
    /// 在C#中实现策略模式（‌Strategy Pattern）‌主要涉及定义一系列的算法，‌并将每一个算法封装起来，‌使它们可以相互替换。‌策略模式让算法的变化独立于使用算法的客户。‌在数据采集系统中，‌策略模式可以用于处理不同类型的数据采集策略。‌
    /// 以下是一个简单的策略模式实现示例，‌假设我们有一个数据采集系统，‌需要根据不同的策略来采集数据：‌
    /// </summary>

    /// <summary>
    /// 1.‌定义策略接口
    /// </summary>
    public interface IDataCollectionStrategy
    {
        void CollectData();
    }

}

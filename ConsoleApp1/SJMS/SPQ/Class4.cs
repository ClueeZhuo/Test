﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.SJMS.SPQ
{
    /// <summary>
    /// 4.‌使用示例
    /// 在这个例子中，‌IExistingDataCollector 和 ExistingDataCollector 代表了现有的数据采集器和它的实现。‌INewDataCollector 是新数据采集器的接口，‌它要求实现一个 CollectNewData 方法。‌
    /// 然而，‌现有的数据采集器并不兼容这个新接口。‌为了解决这个问题，‌我们创建了一个 DataCollectorAdapter 类，‌它实现了新数据采集器的接口，‌并在其内部使用了现有数据采集器的实例。‌
    /// 这样，‌当调用 CollectNewData 方法时，‌适配器会调用现有数据采集器的方法，‌并可能进行一些额外的处理或转换，‌以使数据符合新系统的要求。‌
    /// 通过适配器模式，‌我们可以在不修改现有代码的情况下，‌使不兼容的接口能够一起工作，‌从而提高了代码的可复用性和灵活性。‌
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            IExistingDataCollector existingCollector = new ExistingDataCollector();
            INewDataCollector newCollector = new DataCollectorAdapter(existingCollector);

            newCollector.CollectNewData();
        }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Test
{
    public class Class1
    {
        void Test20()
        {
            var p1 = new List<Person>()
    {
        new Person()
        {
            Age = 1,
        },
        new Person()
        {
            Age = 2,  Name = "a"
        },
        new Person()
        {
            Age = 3,  Name = "B"
        }
    };
            Person p2 = null;
            //Person p2 = new Person()
            //{
            //    Age = 2,
            //    //Name = "A"
            //};

            var p11 = p1.FirstOrDefault(x => x.Name?.ToUpper() == p2.Name?.ToUpper());

            XTrace.WriteLine(p11.ToJson());

        }
        void Test19()
        {
            string abc = "abc";
            abc ??= "def";

            XTrace.WriteLine(abc);
        }

        void Test18()
        {
            var ocrValue = "00:58";
            Console.WriteLine($"ocrValue: {ocrValue}");
            string timeString = ocrValue;//"00:59"; // 时间字符串
            TimeSpan timeSpan;

            // 尝试将时间字符串解析为 TimeSpan 对象
            if (TimeSpan.TryParseExact(timeString, "mm\\:ss", CultureInfo.InvariantCulture, out timeSpan))
            {
                // TimeSpan.Minutes 和 TimeSpan.Seconds 分别提供了分钟和秒数
                //int minutes = timeSpan.Minutes;
                //int seconds = timeSpan.Seconds;

                double seconds = timeSpan.TotalSeconds;

                // 输出秒数
                Console.WriteLine("Seconds: " + seconds);

            }
        }


        void Test17()
        {
            using (var httpClient = new HttpClient())
            {

            }
        }

        void Test16()
        {
            var _TOTAL_IS_CLOSE_RATE = ((2 * 1.0) / (146 * 1.0)) * 100;
            _TOTAL_IS_CLOSE_RATE = ((long)(_TOTAL_IS_CLOSE_RATE * 10000) / 10000.0).ToString().ToDouble();

            XTrace.WriteLine(_TOTAL_IS_CLOSE_RATE.ToString());
        }

        void Test15()
        {
            var dic = new Dictionary<string, string>
    {
        { "T1", "101" },
        { "T2", "102" }
    };

            XTrace.WriteLine(dic["T3"]);
        }
        void Test14()
        {
            List<Person> people = new List<Person>
    {
            new Person { Name = "John", Age = 20, Country = "A" },
            new Person { Name = "Sara", Age = 20, Country = "B" },
            new Person { Name = "Dave", Age = 30, Country = "A" },
            new Person { Name = "Barbara", Age = 25, Country = "C" },
            new Person { Name = "Bill", Age = 35, Country = "A" }
        };

            var sortedPeople = people.OrderBy(p => p.Country)
                                     .ThenBy(p => p.Age)
                                     .ThenBy(p => p.Name);

            foreach (var p in sortedPeople)
            {
                XTrace.WriteLine($" {p.Country}-{p.Name}-{p.Age}");
            }
        }


        void Test13()
        {
            /*
            // 创建一个动态类
            TypeBuilder tb = AppDomain.CurrentDomain.DefineDynamicAssembly(new AssemblyName("MyDynamicAssembly"), AssemblyBuilderAccess.Run)
                            .DefineDynamicModule("MyDynamicModule").DefineType("MyDynamicClass", TypeAttributes.Public);

            // 添加一个属性
            PropertyBuilder pb = tb.DefineProperty("MyProperty", PropertyAttributes.None, typeof(string), null);

            // 定义Getter方法
            MethodBuilder getter = tb.DefineMethod("get_MyProperty", MethodAttributes.Public | MethodAttributes.SpecialName | MethodAttributes.HideBySig, typeof(string), Type.EmptyTypes);
            ILGenerator getIL = getter.GetILGenerator();
            getIL.Emit(OpCodes.Ldarg_0);
            getIL.Emit(OpCodes.Ret);

            // 定义Setter方法
            MethodBuilder setter = tb.DefineMethod("set_MyProperty", MethodAttributes.Public | MethodAttributes.SpecialName | MethodAttributes.HideBySig, null, new Type[] { typeof(string) });
            ILGenerator setIL = setter.GetILGenerator();
            setIL.Emit(OpCodes.Ldarg_0);
            setIL.Emit(OpCodes.Ldarg_1);
            setIL.Emit(OpCodes.Stfld, tb.DefineField("_myField", typeof(string), FieldAttributes.Private));
            setIL.Emit(OpCodes.Ret);

            // 关联Getter和Setter方法
            pb.SetGetMethod(getter);
            pb.SetSetMethod(setter);

            // 创建类型
            Type dynamicType = tb.CreateType();

            // 创建实例并赋值
            object instance = Activator.CreateInstance(dynamicType);
            dynamicType.GetProperty("MyProperty").SetValue(instance, "Hello World");

            // 输出属性值
            Console.WriteLine(dynamicType.GetProperty("MyProperty").GetValue(instance)); // 输出 "Hello World"
            */
        }

        void Test12()
        {
            var list1 = new List<string>() { "A", "B", "C" };
            var list2 = new List<string>() { "1", "2", "3" };
            List<object> list3 = new List<object>();
            for (int i = 0; i < list1.Count; i++)
            {
                var key = list1[i];
                var value = list2[i];
                var anonymous = new { key = value };
                list3.Add(anonymous);
            }

            foreach (var item in list2)
            {

            }

            XTrace.WriteLine(list3.ToJson());


        }

        void Test11()
        {
            var str1 = true.ToString();
            if (str1 == "TRUE")
            {
                XTrace.WriteLine("TRUE");
            }
            else
            {
                XTrace.WriteLine("FALSE");
            }

        }
    }
}

﻿using Hprose.RPC;

using System;
using System.Diagnostics;
using System.Net;

namespace HproseServer
{
    public class TestService : ITestService
    {
        public string Hello(string name, ServiceContext context)
        {
            var endPoint = context.RemoteEndPoint as IPEndPoint;

            return $"Hello {name} from {endPoint.Address}:{endPoint.Port}";
        }

        
        public int Sum(int x, int y)
        {
            return x + y;
        }
    }

    internal class Program
    {
        static void Main(string[] args)
        {
            Trace.Listeners.Add(new TextWriterTraceListener(Console.Out));

            HttpListener server = new HttpListener();
            server.Prefixes.Add("http://127.0.0.1:8080/");
            server.Start();

            var service = new Service();
            //service.Use(Log.IOHandler)
            //       .Use(Log.InvokeHandler)
            //       .AddInstanceMethods(new TestService())
            //       .Bind(server);

            service.AddInstanceMethods(new TestService())
                   .Bind(server);

            Console.WriteLine("服务已启动");

            Console.ReadKey();
            server.Stop();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.SJMS.CL
{
    /// <summary>
    /// 3.‌定义上下文类
    /// </summary>
    public class DataCollector
    {
        private IDataCollectionStrategy _strategy;

        public DataCollector(IDataCollectionStrategy strategy)
        {
            _strategy = strategy;
        }

        public void SetStrategy(IDataCollectionStrategy strategy)
        {
            _strategy = strategy;
        }

        public void Collect()
        {
            _strategy.CollectData();
        }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.SJMS.CXGC
{
    /// <summary>
    /// 4.‌实现具体工厂
    /// </summary>
    public class SensorDataFactory : IDataFactory
    {
        public IDataCollector CreateDataCollector()
        {
            return new SensorDataCollector();
        }

        public IDataProcessor CreateDataProcessor()
        {
            return new SensorDataProcessor();
        }
    }

    public class DatabaseDataFactory : IDataFactory
    {
        public IDataCollector CreateDataCollector()
        {
            return new DatabaseDataCollector();
        }

        public IDataProcessor CreateDataProcessor()
        {
            return new DatabaseDataProcessor();
        }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.SJMS.CL
{
    /// <summary>
    /// 2.‌实现具体的策略
    /// </summary>
    public class SensorDataCollectionStrategy : IDataCollectionStrategy
    {
        public void CollectData()
        {
            Console.WriteLine("Collecting data from sensors.");
        }
    }

    public class DatabaseDataCollectionStrategy : IDataCollectionStrategy
    {
        public void CollectData()
        {
            Console.WriteLine("Collecting data from database.");
        }
    }

}

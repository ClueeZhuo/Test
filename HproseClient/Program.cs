﻿using Hprose.RPC;

using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace HproseClient
{
    public interface ITestService
    {
        Task<string> Hello(string name);

        //[Log(false)]
        Task<int> Sum(int x, int y);
    }

    internal class Program
    {
        static void Main(string[] args)
        {
            Trace.Listeners.Add(new TextWriterTraceListener(Console.Out));

            while (true)
            {
                RunClient().Wait();

                //Thread.Sleep(500);
            }
        }

        static async Task RunClient()
        {
            var client = new Client("http://127.0.0.1:8080/");
            //client.Use(Log.InvokeHandler);

            var proxy = client.UseService<ITestService>();

            Console.WriteLine(await proxy.Hello("world"));
            Console.WriteLine(await proxy.Sum(1, 2));
        }
    }


}

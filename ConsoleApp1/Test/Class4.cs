﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Test
{
    public class Class4
    {
        void Test50()
        {
            var str1 = ",A,B,";

            XTrace.WriteLine(str1.Trim(','));
        }


        void Test49()
        {
            var messageType = "0";

            if (Enum.TryParse(messageType, out EnumMessageType result))
            {
                XTrace.WriteLine($"SUCCESS:{result.ToString()}");
            }
            else
            {
                XTrace.WriteLine($"FAIL");
            }
        }

        void Test48()
        {
            var charA = 'A';
            var char2 = (char)2;
            var char3 = (char)3;

            XTrace.WriteLine($"{charA}");
            XTrace.WriteLine($"{char2}");
            XTrace.WriteLine($"{char3}");

        }

        void Test47()
        {
            var _Now1 = DateTime.Now;

            var _Now2 = _Now1.AddSeconds(3);

            XTrace.WriteLine((_Now2 - _Now1).TotalMilliseconds.ToString());
        }

        void Test46()
        {
            XTrace.WriteLine($"Test46-ThreadId-{Thread.CurrentThread.ManagedThreadId}-1");

            Task.Run(async () =>
            {
                XTrace.WriteLine($"Task-ThreadId-{Thread.CurrentThread.ManagedThreadId}-1");

                await Task.Delay(3000);

                XTrace.WriteLine($"Task-ThreadId-{Thread.CurrentThread.ManagedThreadId}-2");
            });

            XTrace.WriteLine($"Test46-ThreadId-{Thread.CurrentThread.ManagedThreadId}-2");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.SJMS.GCZ
{
    /// <summary>
    /// 3.‌定义主体类
    /// </summary>
    public class DataCollector
    {
        private List<IObserver> observers = new List<IObserver>();
        private float data;

        public void RegisterObserver(IObserver observer)
        {
            observers.Add(observer);
        }

        public void RemoveObserver(IObserver observer)
        {
            observers.Remove(observer);
        }

        public void NotifyObservers()
        {
            foreach (var observer in observers)
            {
                observer.Update(data);
            }
        }

        public void CollectData(float newData)
        {
            this.data = newData;
            NotifyObservers();
        }
    }


}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.SJMS.ZRL
{
    /// <summary>
    /// 3.‌设置责任链并使用
    /// 在这个例子中，‌IDataHandler 接口定义了一个 HandleData 方法和一个 NextHandler 属性。‌SensorDataHandler 和 DatabaseDataHandler 类实现了这个接口，‌并分别处理大于10.0和小于等于10.0的数据。‌如果数据不符合当前处理器的处理条件，‌它会将请求传递给链中的下一个处理器。‌
    /// 通过责任链模式，‌我们可以灵活地添加或删除处理器，‌而不需要修改客户端代码。‌这使得数据采集系统更加模块化和可扩展。‌
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            IDataHandler sensorHandler = new SensorDataHandler();
            IDataHandler databaseHandler = new DatabaseDataHandler();

            sensorHandler.NextHandler = databaseHandler;

            sensorHandler.HandleData(5.0f);
            sensorHandler.HandleData(15.0f);
        }
    }

}

﻿using NewLife.Data;
using NewLife.Log;
using NewLife.Model;
using NewLife.Net;

namespace ConsoleApp1
{
    public class EchoHandler : Handler
    {
        /// <summary>性能计数器</summary>
        public ICounter Counter { get; set; }

        public override Object Read(IHandlerContext context, Object message)
        {
            var ctx = context as NetHandlerContext;
            var session = ctx.Session;

            // 性能计数
            Counter?.Increment(1, 0);

            var pk = message as Packet;

            session.WriteLog("收到：{0}", pk.ToStr());

            XTrace.WriteLine("收到：{0}", pk.ToStr());

            var actor = new MessageActor();
            actor.Tell(pk.ToStr());
            // 等待最终完成
            actor.Stop();

            // 把收到的数据发回去
            //session.SendMessage(pk);

            //return null;

            return base.Read(context, message);
        }
    }
}

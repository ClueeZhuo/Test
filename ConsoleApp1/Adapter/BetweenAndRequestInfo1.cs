﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TouchSocket.Core;

namespace ConsoleApp1.Adapter
{
    /// <summary>
    /// 以**12##12##，Min=5为例。
    /// </summary>
    public class BetweenAndRequestInfo1 : IBetweenAndRequestInfo
    {
        public byte[] Body { get; private set; }

        public void OnParsingBody(ReadOnlySpan<byte> body)
        {
            this.Body = body.ToArray();
        }

        public bool OnParsingEndCode(ReadOnlySpan<byte> endCode)
        {
            return true;//该返回值决定，是否执行Receive
        }

        public bool OnParsingStartCode(ReadOnlySpan<byte> startCode)
        {
            return true;
        }
    }
}

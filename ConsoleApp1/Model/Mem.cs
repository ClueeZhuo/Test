﻿using InfluxDB.Client.Core;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Model
{
    [Measurement("mem")]
    class Mem
    {
        [Column("host", IsTag = true)] public string Host { get; set; }
        [Column("used_percent")] public double? UsedPercent { get; set; }
        [Column(IsTimestamp = true)] public DateTime Time { get; set; }
    }
}

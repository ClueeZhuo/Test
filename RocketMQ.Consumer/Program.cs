﻿using NewLife;
using NewLife.Log;
using NewLife.RocketMQ;

using System.Diagnostics;

try
{
    XTrace.UseConsole();

    var consumer = new Consumer
    {
        Topic = "SXHDMES_OPENMES_CS",
        FromLastOffset = true,
        BatchSize = 20,
        Log = XTrace.Log,
    };
    consumer.Configure(MqSetting.Current);

    consumer.OnConsume = (q, ms) =>
    {
        XTrace.WriteLine("[{0}@{1}]收到消息[{2}]", q.BrokerName, q.QueueId, ms.Length);

        foreach (var item in ms.ToList())
        {
            XTrace.WriteLine($"消息：主键【{item.Keys}】，产生时间【{item.BornTimestamp.ToDateTime()}】，内容【{item.Body.ToStr()}】");
        }

        return true;
    };

    consumer.Start();
}
catch (Exception exp)
{
    XTrace.WriteException(exp);
}
finally
{
    Console.ReadKey();
}
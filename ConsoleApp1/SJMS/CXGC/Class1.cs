﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.SJMS.CXGC
{
    /// <summary>
    /// 抽象工厂模式
    /// “抽象工厂模式”（‌Abstract Factory Pattern）‌，‌它提供了一个接口，‌用于创建相关或依赖对象的家族，‌而不需要明确指定具体类。‌
    /// 以下是一个抽象工厂模式的例子，‌适用于一个数据采集系统，‌其中可能需要根据不同的数据源创建不同的数据采集器和处理器：‌
    /// </summary>

    /// <summary>
    /// 1.‌定义抽象产品
    /// </summary>

    public interface IDataCollector
    {
        void CollectData();
    }

    public interface IDataProcessor
    {
        void ProcessData();
    }


}

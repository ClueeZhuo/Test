﻿namespace MQTT
{
    public class PostData
    {
        /// <summary>
        /// 关键key，MES收到同样的Key不会进行数据处理
        /// </summary>
        public string MESSAGE_ID { get; set; }
        /// <summary>
        /// 来源系统
        /// </summary>
        public string SOURCE_SYSTEM { get; set; }
        /// <summary>
        /// 消息类型
        /// </summary>
        public string MESSAGE_TYPE { get; set; }
        /// <summary>
        /// 关键数据
        /// </summary>
        public string KEY_CONTENT { get; set; }
        /// <summary>
        /// 消息产生时间，由对方系统产生
        /// </summary>
        public DateTime MESSAGE_TIME { get; set; }
        /// <summary>
        /// 接收的数据内容
        /// </summary>
        public object CONTENT { get; set; }
    }
}

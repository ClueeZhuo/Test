﻿using Hprose.RPC;

namespace HproseServer
{
    public interface ITestService
    {
        string Hello(string name, ServiceContext context);
        int Sum(int x, int y);
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.SJMS.SPQ
{
    /// <summary>
    /// 适配器模式
    /// 在C#数采系统中，‌适配器模式（‌Adapter Pattern）‌是一种设计模式，‌它允许将不兼容的接口转换为另一个接口，‌从而使原本由于接口不兼容而不能一起工作的类可以一起工作。‌以下是一个简单的适配器模式实现示例，‌适用于数据采集系统：‌
    /// </summary>


    /// <summary>
    /// 1.‌定义现有数据采集器的接口和实现
    /// </summary>
    public interface IExistingDataCollector
    {
        string CollectExistingData();
    }

    public class ExistingDataCollector : IExistingDataCollector
    {
        public string CollectExistingData()
        {
            return "Existing data collected.";
        }
    }

}

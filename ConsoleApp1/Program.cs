﻿using NAudio.Wave;

using System.Media;



try
{
    XTrace.UseConsole();
    XTrace.WriteLine("START");

    //while (true)
    //{
    //    "机型A B C转为D E F".SpeakAsync();

    //    Thread.Sleep(2000);
    //}

    Task.Run(async () => { await new NAudioService().Test3(); });

    //Test1();
}
catch (Exception exp)
{
    XTrace.WriteException(exp);
}
finally
{
    XTrace.WriteLine("END");
    Console.ReadLine();
}

void Test1()
{
    var time = 1;
    while (true)
    {
        try
        {
            SoundPlayer player = new SoundPlayer();
            //player.SoundLocation = $"https://inner-gw.hisense.com/hiai/tts?user_key=05d4388fc6d3a44ddfa1dae9ebfe813d&type=wav&text=当前生产机型ABC批次{time}";

            player.SoundLocation = $"https://inner-gw.hisense.com/hiai/tts?user_key=05d4388fc6d3a44ddfa1dae9ebfe813d&type=wav&text=ABC{time}";

            player.Load();
            player.Play();

            time++;
            Thread.Sleep(5000);
        }
        catch (Exception ex)
        {
            XTrace.WriteException(ex);
        }
    }
}

void Test2()
{
    var time = 1;
    while (true)
    {
        try
        {
            string audioUrl = $"https://inner-gw.hisense.com/hiai/tts?user_key=05d4388fc6d3a44ddfa1dae9ebfe813d&type=wav&text=当前生产机型ABC批次{time}";

            using (HttpClient client = new HttpClient())
            {
                Console.WriteLine("开始下载音频...");
                var response = client.GetAsync(audioUrl).Result;
                if (!response.IsSuccessStatusCode)
                {
                    Console.WriteLine($"请求失败，状态码: {response.StatusCode}");
                    return;
                }
                byte[] audioData = response.Content.ReadAsByteArrayAsync().Result;
                Console.WriteLine("音频下载完成。");

                using (MemoryStream stream = new MemoryStream(audioData))
                {
                    using (Mp3FileReader mp3Reader = new Mp3FileReader(stream))
                    {
                        using (WaveOutEvent waveOut = new WaveOutEvent())
                        {
                            waveOut.Init(mp3Reader);
                            waveOut.Play();
                            //Console.WriteLine("音频正在播放，按任意键停止...");
                            //Console.ReadKey();

                            Thread.Sleep(5000);
                            waveOut.Stop();
                        }
                    }
                }
            }

            time++;
            Thread.Sleep(2000);
        }
        catch (Exception ex)
        {
            XTrace.WriteException(ex);
        }
    }
}



﻿namespace WindowsFormsApp1
{
    partial class ucFunctionIco
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.Title = new System.Windows.Forms.Label();
            this.PicBox_Bool = new System.Windows.Forms.PictureBox();
            this.PicBox_Img = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.PicBox_Bool)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBox_Img)).BeginInit();
            this.SuspendLayout();
            // 
            // Title
            // 
            this.Title.Font = new System.Drawing.Font("宋体", 9F);
            this.Title.Location = new System.Drawing.Point(3, 67);
            this.Title.Name = "Title";
            this.Title.Size = new System.Drawing.Size(100, 37);
            this.Title.TabIndex = 1;
            this.Title.Text = "测试测试测试测试测试";
            this.Title.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // PicBox_Bool
            // 
            this.PicBox_Bool.Location = new System.Drawing.Point(62, 16);
            this.PicBox_Bool.Name = "PicBox_Bool";
            this.PicBox_Bool.Size = new System.Drawing.Size(10, 10);
            this.PicBox_Bool.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PicBox_Bool.TabIndex = 2;
            this.PicBox_Bool.TabStop = false;
            // 
            // PicBox_Img
            // 
            this.PicBox_Img.Location = new System.Drawing.Point(30, 20);
            this.PicBox_Img.Name = "PicBox_Img";
            this.PicBox_Img.Size = new System.Drawing.Size(40, 40);
            this.PicBox_Img.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PicBox_Img.TabIndex = 0;
            this.PicBox_Img.TabStop = false;
            this.PicBox_Img.Click += new System.EventHandler(this.ucFunctionIco_Click);
            // 
            // ucFunctionIco
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.PicBox_Bool);
            this.Controls.Add(this.Title);
            this.Controls.Add(this.PicBox_Img);
            this.Name = "ucFunctionIco";
            this.Size = new System.Drawing.Size(100, 100);
            this.Click += new System.EventHandler(this.ucFunctionIco_Click);
            ((System.ComponentModel.ISupportInitialize)(this.PicBox_Bool)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PicBox_Img)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox PicBox_Img;
        private System.Windows.Forms.Label Title;
        private System.Windows.Forms.PictureBox PicBox_Bool;
    }
}

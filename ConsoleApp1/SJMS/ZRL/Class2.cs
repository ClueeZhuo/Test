﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.SJMS.ZRL
{
    /// <summary>
    /// 2.‌实现具体的处理器
    /// </summary>
    public class SensorDataHandler : IDataHandler
    {
        public IDataHandler NextHandler { get; set; }

        public void HandleData(float data)
        {
            if (data > 10.0f)
            {
                Console.WriteLine("Sensor data handler processed data: " + data);
            }
            else if (NextHandler != null)
            {
                NextHandler.HandleData(data);
            }
        }
    }

    public class DatabaseDataHandler : IDataHandler
    {
        public IDataHandler NextHandler { get; set; }

        public void HandleData(float data)
        {
            if (data <= 10.0f)
            {
                Console.WriteLine("Database data handler processed data: " + data);
            }
            else if (NextHandler != null)
            {
                NextHandler.HandleData(data);
            }
        }
    }

}

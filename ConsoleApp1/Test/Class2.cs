﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Test
{
    public class Class2
    {
        void Test30()
        {
            //using var client = InfluxDBClientFactory.Create(InfluxUrl, Token);

            //for (global::System.Int32 i = 1; i < 1000; i++)
            //{
            //    var mem = new Mem { Host = "host1", UsedPercent = i, Time = DateTime.UtcNow };
            //    var mem2 = new Mem { Host = "host1", UsedPercent = i * 2, Time = DateTime.UtcNow };

            //    var mems = new List<Mem>() { mem, mem2 };


            //    using (var writeApi = client.GetWriteApi())
            //    {
            //        writeApi.WriteMeasurement(mem, WritePrecision.Ns, Bucket, Org);
            //        //writeApi.WriteMeasurements(mems, WritePrecision.Ns, Bucket, Org);
            //    }

            //    Thread.Sleep(2000);
            //}
        }



        void Test29()
        {
            List<Person> list1 = new List<Person>
    {
        new Person { Name = "张三", Age = 1,Country="CHINA" },
        new Person { Name = "李四", Age = 2,Country="CHINA" },
        new Person { Name = "王五", Age = 3,Country="USA" },
    };

            var dct = list1.GroupBy(p => p.Country)
                    .ToDictionary(g => g.Key, g => g.ToList()); ;

            XTrace.WriteLine(dct.ToJson());
        }

        void Test28()
        {
            XTrace.WriteLine(new Guid("06172822-5B7F-4751-9126-B5AAC6925B24").ToString());
        }
        void Test27()
        {
            for (int i = 300; i <= 338; i += 2)
            {
                XTrace.WriteLine("D" + i.ToString("D3"));
            }


        }
        void Test26()
        {
            List<Person> myList = new List<Person>
        {
            new Person { Name = "A", Age = 1 },
            new Person { Name = "B", Age = 2 },
            new Person { Name = "C", Age = 3 },
            new Person { Name = "B", Age = 4 },
            new Person { Name = "D", Age = 5 },
            new Person { Name = "B", Age = 6 },
            new Person { Name = "E", Age = 7 },
            new Person { Name = "F", Age = 8 },
            new Person { Name = "G", Age = 9 },
            new Person { Name = "A", Age = 10 },
        };

            // 使用LINQ进行升序排序并创建新列表
            var list2 = myList.OrderBy(A => A.Age).Select(A => A.Name).ToHashSet();

            // 首先，创建一个字典来映射List2中每个对象的索引
            var indexMap = list2.Select((obj, index) => new { Object = obj, Index = index })
                                 .ToDictionary(pair => pair.Object, pair => pair.Index);

            List<Person> list1 = new List<Person>
    {
        new Person { Name = "A", Age = 1 },
        new Person { Name = "F", Age = 8 },
            new Person { Name = "C", Age = 3 },
        new Person { Name = "B", Age = 2 },
            new Person { Name = "D", Age = 5 },
            new Person { Name = "G", Age = 9 },
            new Person { Name = "H", Age = 9 },
            new Person { Name = "E", Age = 7 },
            new Person { Name = "I", Age = 9 },
        };

            // 然后，使用LINQ的OrderBy对List1进行排序，排序的依据是List2中每个对象的索引
            // 使用LINQ对list1进行排序
            var sortedList = list1.OrderBy(obj1 =>
            {
                // 如果obj1在list2中，返回其在list2中的索引
                if (indexMap.ContainsKey(obj1.Name))
                {
                    return indexMap[obj1.Name];
                }
                // 如果obj1不在list2中，返回一个大于list2长度的值，以确保它排在最后
                return list2.Count;
            }).ToList();


            // 输出排序后的List1
            foreach (var obj in sortedList)
            {
                Console.WriteLine(obj.Name);
            }


        }

        void Test25()
        {
            var a = 10;
            var b = "10";


            XTrace.WriteLine($"a==b?{a.Equals(b)}");

        }

        void Test24()
        {
            var a = 101;
            var b = a * 0.7;

            XTrace.WriteLine(b.ToString());

        }

        void Test23()
        {
            SerialPortHealthChecker checker = new SerialPortHealthChecker("COM3"); // 替换为你的串口名称

            while (true)
            {
                // 尝试打开串口
                if (checker.TryOpenPort())
                {
                    // 检查串口配置
                    if (checker.CheckPortSettings(9600, Parity.None, 8, StopBits.One))
                    {
                        // 测试通信
                        if (checker.TestCommunication())
                        {
                            XTrace.WriteLine("串口通信正常。");
                        }
                        else
                        {
                            XTrace.WriteLine("串口通信异常。");
                        }
                    }
                }

                Thread.Sleep(2000);
            }
            // 关闭串口
            checker.ClosePort();
        }

        void Test22()
        {
            XTrace.WriteLine(DateTime.Now.AddMonths(6).ToString("yy年M月"));
        }

        void Test21()
        {
            if (int.TryParse("1.1", out int result1))
            {
                XTrace.WriteLine(result1.ToString());
            }
            else
            {
                XTrace.WriteLine("F");
            }
        }
    }
}

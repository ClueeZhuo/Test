﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.SJMS.SPQ
{
    /// <summary>
    /// 3.‌创建适配器类
    /// </summary>
    public class DataCollectorAdapter : INewDataCollector
    {
        private IExistingDataCollector existingCollector;

        public DataCollectorAdapter(IExistingDataCollector existingCollector)
        {
            this.existingCollector = existingCollector;
        }

        public void CollectNewData()
        {
            // 适配逻辑：‌调用现有数据采集器的方法，‌并可能进行一些转换或处理
            string existingData = existingCollector.CollectExistingData();
            Console.WriteLine($"Adapter collected data: {existingData}");
            // 这里可以添加额外的逻辑来处理或转换数据
        }
    }

}

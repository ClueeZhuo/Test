﻿using NewLife.Model;

namespace ConsoleApp1.Test
{
    internal class Class8
    {

        public static void Test82()
        {
            var ts = DateTime.Now - new DateTime(1970, 1, 1, 0, 0, 0, 0);

            XTrace.WriteLine(((DateTimeOffset)DateTime.UtcNow).ToUnixTimeSeconds().ToString());
        }

        public static async Task Test81()
        {
            var sw = Stopwatch.StartNew();

            var actor = new BuildExcelActor();

            for (var i = 0; i < 6; i++)
            {
                // 模拟查询数据，耗时500ms
                XTrace.WriteLine("读取数据i……");
                await Task.Delay(500);

                // 通知另一个处理器
                actor.Tell($"数据行i_{i + 1}");
            }

            // 等待最终完成
            actor.Stop();

            for (var i = 0; i < 6; i++)
            {
                // 模拟查询数据，耗时500ms
                XTrace.WriteLine("读取数据j……");
                await Task.Delay(500);

                // 通知另一个处理器
                actor.Tell($"数据行j_{i + 1}");
            }
            actor.Stop();

            sw.Stop();
        }
    }
    public class BuildExcelActor : Actor
    {
        protected override async Task ReceiveAsync(ActorContext context, CancellationToken cancellationToken)
        {
            XTrace.WriteLine("生成Excel数据：{0}", context.Message);

            await Task.Delay(500);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Test
{
    public class Class3
    {
        void Test40()
        {

            XTrace.WriteLine("123000000   "?.Trim()?.TrimEnd('0'));
        }

        void Test39()
        {
            // 创建一个ParameterPair对象的列表
            List<ParameterPair> parameters = new List<ParameterPair>
    {
        new ParameterPair("SERIAL_NUMBER", "1TE8544TCNCB0193P480476"),
        new ParameterPair("BOM", "1"),
        new ParameterPair("MODEL_CODE", "85D3N"),
        new ParameterPair("RFID", "T48-ZP10")
    };

            XTrace.WriteLine(parameters.ToJson());
        }

        void Test38()
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            Thread.Sleep(3000);
            stopwatch.Stop();
            XTrace.WriteLine(stopwatch.Elapsed.TotalMilliseconds.ToString());

            byte length1 = byte.Parse("8");
            byte length2 = (byte)8;

            XTrace.WriteLine($"{length1}=={length2}? {length1 == length2}");
        }

        void Test37()
        {
            string originalString = "\u0000This is a test string with \u0000 embedded null characters\u0000";

            string replacedString = originalString.Replace('\0', ' ');

            XTrace.WriteLine(replacedString);
        }

        void Test36()
        {
            byte myByte = 0;
            char myChar = (char)myByte;
            XTrace.WriteLine($"A{myChar}B");
        }

        void Test35()
        {
            XTrace.WriteLine(new string(Enumerable.Repeat(',', 30).ToArray()));

            XTrace.WriteLine("12345".PadRight(10, ','));
        }

        void Test34()
        {
            HslCommunication.MQTT.MqttConnectionOptions options = new HslCommunication.MQTT.MqttConnectionOptions();
            options.IpAddress = "127.0.0.1";
            options.Port = 1883;
            options.ClientId = "";
            options.UseRSAProvider = false;
            options.Credentials = null;
            options.CleanSession = true;
            options.UseSSL = false;
            options.SSLSecure = false;
            options.CertificateFile = "";
            options.KeepAlivePeriod = TimeSpan.FromSeconds(100);
            options.KeepAliveSendInterval = TimeSpan.FromSeconds(30);
            options.WillMessage = null;
            HslCommunication.MQTT.MqttClient mqtt = new HslCommunication.MQTT.MqttClient(options);

            mqtt.OnMqttMessageReceived += Mqtt_OnMqttMessageReceived;
            OperateResult connect = mqtt.ConnectServer();
            if (!connect.IsSuccess)
            {
                Console.WriteLine("Connect failed: " + connect.Message);
                Console.ReadLine();
                return;
            }
            // 订阅
            mqtt.SubscribeMessage("A");

        }

        void Mqtt_OnMqttMessageReceived(HslCommunication.MQTT.MqttClient client, MqttApplicationMessage message)
        {
            // 接收到服务器发送的数据触发的事件，这里就是先显示下
            Console.WriteLine(message.ToString());
        }

        const string InfluxUrl = "http://127.0.0.1:8086";
        const string Token = "l7fPhihBIxQSjJ_1YyuNOf59Q1gJeauh53t4mIWC_KkQIGTPz_HclBockQxaGoxYFADwSIKU4Sy7UIG0nK5cmQ==";
        const string Org = "HISENSE";
        const string Bucket = "IOTGATEWAY";

        static void Test33()
        {
            var dto = new packdto { car = "苏U.ddddd", money = 123.488, temp = 123, tempCn = "321", Timestamp = DateTime.UtcNow };
            var json = dto.ToJson();
            var dto1 = json.ToJsonEntity<packdto>();

            using (var client = InfluxDBClientFactory.Create(InfluxUrl, Token))
            {
                using (var writeApi = client.GetWriteApi())
                {
                    writeApi.WriteMeasurement(dto1, WritePrecision.Ns, Bucket, Org);
                }
            }
        }

        static void Test32()
        {
            using (var client = InfluxDBClientFactory.Create(InfluxUrl, Token))
            {

                var point = PointData
                   .Measurement("pack1")
                   .Tag("car", "苏E.P6M88")
                   .Field("money", 28.43)
                   .Field("temp", 23)
                   .Field("raw", "1")
                   .Timestamp(DateTime.UtcNow, WritePrecision.Ns);

                var point2 = PointData
                  .Measurement("pack1")
                  .Tag("car", "苏E.P6M66")
                  .Field("money", 28.43)
                  .Field("temp", 28)
                  .Field("raw", "22")
                  .Timestamp(DateTime.UtcNow, WritePrecision.Ns);

                var point3 = PointData
                .Measurement("pack1")
                .Tag("car", "苏U.P6M22")
                .Field("money", 28.43)
                .Field("temp", 32)
                .Field("raw", "333")
                .Timestamp(DateTime.UtcNow, WritePrecision.Ns);

                var point4 = PointData
                   .Measurement("pack1")
                   .Tag("car", "苏U.P6M55")
                   .Field("money", 100)
                   .Field("temp", 42)
                   .Field("raw", "4444")
                   .Timestamp(DateTime.UtcNow, WritePrecision.Ns);

                using (var writeApi = client.GetWriteApi())
                {
                    writeApi.WritePoint(point, Bucket, Org);
                    writeApi.WritePoint(point2, Bucket, Org);
                    writeApi.WritePoint(point3, Bucket, Org);
                    writeApi.WritePoint(point4, Bucket, Org);
                }
            }

        }

        async Task Test31()
        {
            using (var client = InfluxDBClientFactory.Create(InfluxUrl, Token))
            {
                var query = from s in InfluxDBQueryable<packdto>.Queryable(Bucket, Org, client.GetQueryApiSync())
                            orderby s.Timestamp
                            select s;
                var datas = query.ToList();

            }
        }
    }
}

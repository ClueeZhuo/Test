﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.SJMS.SPQ
{
    /// <summary>
    /// 2.‌定义新数据采集器的接口
    /// </summary>
    public interface INewDataCollector
    {
        void CollectNewData();
    }

}

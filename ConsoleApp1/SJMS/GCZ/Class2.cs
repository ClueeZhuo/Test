﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.SJMS.GCZ
{
    /// <summary>
    /// 2.‌实现具体的观察者
    /// </summary>
    public class DataDisplay : IObserver
    {
        public void Update(float data)
        {
            Console.WriteLine($"Received data: {data}");
        }
    }


}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.SJMS.GCZ
{
    /// <summary>
    /// 4.‌使用示例
    /// 在这个例子中，‌IObserver 接口定义了一个 Update 方法，‌用于接收数据更新。‌DataDisplay 类实现了这个接口，‌用于显示接收到的数据。‌DataCollector 类是主体类，‌它维护了一个观察者列表，‌并在数据更新时通知所有观察者。‌通过 RegisterObserver 和 RemoveObserver 方法，‌可以在运行时添加或移除观察者。‌CollectData 方法用于模拟数据采集，‌并在采集到新数据时通知所有观察者。‌‌
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            DataCollector collector = new DataCollector();
            DataDisplay display = new DataDisplay();

            collector.RegisterObserver(display);

            collector.CollectData(1.23f);
            collector.CollectData(4.56f);
        }
    }


}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.SJMS.CXGC
{
    /// <summary>
    /// 3.‌定义抽象工厂
    /// </summary>
    public interface IDataFactory
    {
        IDataCollector CreateDataCollector();
        IDataProcessor CreateDataProcessor();
    }


}
